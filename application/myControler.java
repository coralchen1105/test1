package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.*;
import javafx.fxml.Initializable;

public class myControler implements Initializable{
	@FXML private Circle circle;
	@FXML private Polygon polygon;
	@FXML private Rectangle rectangle;
	@FXML private Label label1;
	//@FXML private Label label2;
	@FXML private Button bn1;
	@FXML private Button bn2;
	
	private Shape shape;
	
	@FXML protected void handleButtonAction(ActionEvent event) {
       
		bn1.setText("change button");
		System.out.println("You clicked me!");
    }

	
	@FXML protected void circleMouseEntered(){
		
		EventHandler<MouseEvent> mouseEvent = new EventHandler<MouseEvent>(){
			@Override
	          public void handle(MouseEvent arg0) {
				
//	        	  System.out.println(circle.getClass()); 
	        	 circle.setFill(Color.web("#0000FF"));
	        	 label1.setText("Mouse X:" + arg0.getSceneX() + "  Mouse Y" + arg0.getSceneY());
	         }
		};
		
		circle.addEventHandler(MouseEvent.MOUSE_MOVED, mouseEvent);
		circle.addEventHandler(MouseEvent.MOUSE_ENTERED, mouseEvent);

	}
	
	
	
	@FXML protected void circleMouseLeave(){
		EventHandler<MouseEvent> mouseEvent = new EventHandler<MouseEvent>(){
			@Override
	          public void handle(MouseEvent arg0) {
	        	  //System.out.println("Mouse entered"); 
	        	 circle.setFill(Color.web("#fff500"));
	         }
		};
		circle.addEventHandler(MouseEvent.MOUSE_EXITED, mouseEvent);
	}
	
	
@FXML protected void recMouseEntered(){
		
		EventHandler<MouseEvent> mouseEvent = new EventHandler<MouseEvent>(){
			@Override
	          public void handle(MouseEvent arg0) {
				
//	        	  System.out.println(circle.getClass()); 
				rectangle.setFill(Color.web("#0000FF"));
	        	 label1.setText("Mouse X:" + arg0.getSceneX() + "  Mouse Y" + arg0.getSceneY());
	         }
		};
		
		rectangle.addEventHandler(MouseEvent.MOUSE_MOVED, mouseEvent);
		rectangle.addEventHandler(MouseEvent.MOUSE_ENTERED, mouseEvent);

	}
	
	
	
	@FXML protected void recMouseLeave(){
		EventHandler<MouseEvent> mouseEvent = new EventHandler<MouseEvent>(){
			@Override
	          public void handle(MouseEvent arg0) {
	        	  //System.out.println("Mouse entered"); 
				rectangle.setFill(Color.web("#990000"));
	         }
		};
		rectangle.addEventHandler(MouseEvent.MOUSE_EXITED, mouseEvent);
	}
	
@FXML protected void polyMouseEntered(){
		
		EventHandler<MouseEvent> mouseEvent = new EventHandler<MouseEvent>(){
			@Override
	          public void handle(MouseEvent arg0) {
				
//	        	  System.out.println(circle.getClass()); 
				polygon.setFill(Color.web("#0000FF"));
	        	 label1.setText("Mouse X:" + arg0.getSceneX() + "  Mouse Y" + arg0.getSceneY());
	         }
		};
		
		polygon.addEventHandler(MouseEvent.MOUSE_MOVED, mouseEvent);
		polygon.addEventHandler(MouseEvent.MOUSE_ENTERED, mouseEvent);

	}
	
	
	
	@FXML protected void polyMouseLeave(){
		EventHandler<MouseEvent> mouseEvent = new EventHandler<MouseEvent>(){
			@Override
	          public void handle(MouseEvent arg0) {
	        	  //System.out.println("Mouse entered"); 
				polygon.setFill(Color.web("#7a0099"));
	         }
		};
		polygon.addEventHandler(MouseEvent.MOUSE_EXITED, mouseEvent);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
	
}
